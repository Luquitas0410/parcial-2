using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public Camera camaraPrimeraPersona;

    //instanciar poderes
    public Fuego fuego;                //Script
    public Electricidad electricidad;  //Script
    public LineRenderer linerenderer;


    public GameObject arma;
    public GameObject brazoElectricidad; //Brazo
    public GameObject brazoFuego;        //Brazo
    public GameObject padre;
    public Transform SpawnElec;
    public Transform SpawnFuego;

    //instanciar arma
    public Arma1 armascript;
    public Transform SpawnArma1;
    public GameObject pistola;

    public GameObject luz;

    //barra de vida
    public Image barraDeVida;
    public float hpMax;
    public int hp;

    public TMPro.TMP_Text F;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        fuego.enabled = false;
        electricidad.enabled = false;
        linerenderer.enabled = false;
        armascript.enabled = false;

        F.text = "";
    }

    void Update()
    //sistema de movimiento
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

         barraDeVida.fillAmount = hp / hpMax;
            
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        
    }
    //sistema de items
    private void OnTriggerEnter(Collider other)
    {
        //electricidad
        if (other.gameObject.CompareTag("electricidadup") == true)
        {
            other.gameObject.SetActive(false);
            
            GameObject CloneBrazo = Instantiate(brazoElectricidad, SpawnElec.transform.position, Quaternion.identity) as GameObject;
            CloneBrazo.transform.parent = padre.transform;
            CloneBrazo.transform.rotation = padre.transform.rotation;

            GestorDeAudio.instancia.PausarSonido("fuego");
            GestorDeAudio.instancia.ReproducirSonido("electricidadup");
            electricidad.enabled = true;
            linerenderer.enabled = true;
            fuego.enabled = false;
            GameObject.Find("BrazoFuego2(Clone)").SetActive(false);
        }

        //fuego
        if (other.gameObject.CompareTag("fuegoup") == true)
        {
            other.gameObject.SetActive(false);

            GameObject BrazoFuego = Instantiate(brazoFuego, SpawnFuego.transform.position, Quaternion.identity);
            BrazoFuego.transform.parent = padre.transform;
            BrazoFuego.transform.rotation = padre.transform.rotation;
            
            GestorDeAudio.instancia.PausarSonido("electricidad");
            GestorDeAudio.instancia.ReproducirSonido("fuegoup");
            fuego.enabled = true;
            electricidad.enabled = false;
            linerenderer.enabled = false;
            GameObject.Find("BrazoElectricidad(Clone)").SetActive(false);
        }

       
        
        
         if (other.gameObject.CompareTag("accion") == true)
         {
             other.gameObject.SetActive(false);
             Destroy(luz, 0);
            GestorDeAudio.instancia.ReproducirSonido("foquito");
            F.text = "F";
         }
        if (Input.GetKeyDown(KeyCode.F))
        {
            F.text = "";
        }



    }

    //sistema de vida
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("enemy"))
        {
            recibirDaño();
        }
    }

    public void recibirDaño()
    {
        hp = hp - 35;

        if (hp <= 0)
        {
            this.desaparecer();
        }
        GestorDeAudio.instancia.ReproducirSonido("auch");
    }

    public void recibirDañoExplosivo()
    {
        hp = hp - 75;

        if (hp <= 0)
        {
            this.desaparecer();
        }
        GestorDeAudio.instancia.ReproducirSonido("auch");
    }

    private void desaparecer()
    {
        
        GestorDeAudio.instancia.ReproducirSonido("grito muerte");
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.name);
    }

}

  


