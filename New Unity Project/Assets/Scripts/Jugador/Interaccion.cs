using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaccion : MonoBehaviour
{
    public Camera camaraPrimeraPersona;

    public TMPro.TMP_Text gameover;
    public TMPro.TMP_Text respawm;
    public Animator puerta0;

    

    void Start()
    {
        puerta0.SetBool("abrir", false);
    }

    
    void Update()
    {
        if (Input.GetKeyDown("e"))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            
            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 4)
            {
                
                if (hit.collider.name.Substring(0, 5) == "boton")
                {
                    GameObject objetoTocado = GameObject.Find(hit.transform.name);
                    AbrirPuerta scriptObjetoTocado = (AbrirPuerta)objetoTocado.GetComponent(typeof(AbrirPuerta));

                    if (scriptObjetoTocado != null)
                    {
                        scriptObjetoTocado.Abrir();
                    }
                }
            }

            if ((Physics.Raycast(ray, out hit) == false))
            {
                GestorDeAudio.instancia.ReproducirSonido("disparo fallado");
            }

            if (hit.collider.name.Substring(0, 5) == "Grab1")
           
            {
                GameObject objetoTocado = GameObject.Find(hit.transform.name);
                Receptor scriptObjetoTocado = (Receptor)objetoTocado.GetComponent(typeof(Receptor));

                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.Grab1();
                }
            }

            if (hit.collider.name.Substring(0, 5) == "Grab2")

            {
                GameObject objetoTocado = GameObject.Find(hit.transform.name);
                Receptor scriptObjetoTocado = (Receptor)objetoTocado.GetComponent(typeof(Receptor));

                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.Grab2();
                }
            }

            if (hit.collider.name.Substring(0, 5) == "ArmaU")

            {
                GameObject objetoTocado = GameObject.Find(hit.transform.name);
                Receptor scriptObjetoTocado = (Receptor)objetoTocado.GetComponent(typeof(Receptor));

                
                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.Arma();
                }
            }

            if (hit.collider.name.Substring(0, 5) == "puer0")

            {
               GestorDeAudio.instancia.ReproducirSonido("ascensor");
               puerta0.SetBool("abrir", true);
               respawm.text = "";
            }
            if (hit.collider.name.Substring(0, 5) == "boto4")
            {
                gameover.text = "Game Over";

                GameObject objetoTocado = GameObject.Find(hit.transform.name);
                AbrirPuerta scriptObjetoTocado = (AbrirPuerta)objetoTocado.GetComponent(typeof(AbrirPuerta));

                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.Abrir();
                }
            }
            if (hit.collider.name.Substring(0, 5) == "cura0")
            {
                

                GameObject objetoTocado = GameObject.Find(hit.transform.name);
                Receptor scriptObjetoTocado = (Receptor)objetoTocado.GetComponent(typeof(Receptor));

                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.cura();
                }
            }

        }
    }
}

