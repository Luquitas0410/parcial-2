using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Receptor : MonoBehaviour
{
    //instanciar arma
    public Arma1 armascript;
    public Transform SpawnArma1;
    public GameObject pistola;
    public GameObject padre;

    public GameObject ascensor;
    public float velocidad;

    public ControlJugador curar;


    void Start()
    {
        
    }

    
    void Update()
    {
        
    }
    public void Subir()
    {

    }
    public void Grab1()
    {
        Destroy(gameObject, 0);
        GestorDeAudio.instancia.ReproducirSonido("Grabacion1");
    }
    public void Grab2()
    {
        Destroy(gameObject, 0);
        GestorDeAudio.instancia.ReproducirSonido("Grabacion2");
    }

    public void Arma()
    {

         GameObject Pistola = Instantiate(pistola, SpawnArma1.transform.position, Quaternion.identity);
         Pistola.transform.parent = padre.transform;
         Pistola.transform.rotation = padre.transform.rotation;

         armascript.enabled = true;
         GestorDeAudio.instancia.ReproducirSonido("cargar arma");
        Destroy(gameObject, 0);
    }
    
    public void cura()
    {
        Destroy(gameObject, 0);
        GestorDeAudio.instancia.ReproducirSonido("botiquin");

        curar = FindObjectOfType<ControlJugador>();
        curar.hp = curar.hp + 50;
    }
     
}
