using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaltoJugador : MonoBehaviour
{
    public Rigidbody rb;
    public float saltoVel;
    private bool enElSuelo = true;
    public int maxSaltos = 1;
    public int saltoActual = 0;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump") && (enElSuelo || maxSaltos > saltoActual))
        {
            rb.velocity = new Vector3(0f, saltoVel, 0f * Time.deltaTime);
            enElSuelo = false;
            saltoActual++;
            GestorDeAudio.instancia.ReproducirSonido("salto");
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        enElSuelo = true;
        saltoActual = 0;
    }

}