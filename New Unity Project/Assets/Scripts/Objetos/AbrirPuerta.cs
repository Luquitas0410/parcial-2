using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbrirPuerta : MonoBehaviour
{

    public Animator Puerta;
    public GameObject candado;


    void Start()
    {
        Puerta.SetBool("abrir", false);
    }


    void Update()
    {

    } 

    public void Abrir()
    {
        Puerta.SetBool("abrir", true);
        GestorDeAudio.instancia.ReproducirSonido("puerta");
    }
    public void AbrirCandado()
    {
        Puerta.SetBool("abrir", true);
        GestorDeAudio.instancia.ReproducirSonido("puerta");
        Destroy(candado.gameObject);
        GestorDeAudio.instancia.ReproducirSonido("candado");
    }


}
