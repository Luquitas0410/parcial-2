using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Linterna : MonoBehaviour
{
    public Light luzLinterna;
    public bool luzActiva;
    public TMPro.TMP_Text F;

    void Start()
    {
        luzLinterna.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            F.text = "";

            luzActiva = !luzActiva;

            if (luzActiva == true)
            {
                luzLinterna.enabled = true;
                GestorDeAudio.instancia.ReproducirSonido("linternaon");
            }

            if (luzActiva == false)
            {
                luzLinterna.enabled = false;
                GestorDeAudio.instancia.ReproducirSonido("linternaof");
            }
        }
    }
}
