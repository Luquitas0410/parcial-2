using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MisilTeledirigido : MonoBehaviour
{
    
    private GameObject jugador;
    public int rapidez;

    void Start()
    {

       jugador = GameObject.Find("jugador");
    }

    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }


}
