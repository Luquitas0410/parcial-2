using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Daño : MonoBehaviour 
{
    public int hp;
    public GameObject particulamuerte;
    public GameObject padre;
    public GameObject explosionBolaDeFuego;
    public GameObject bot;

    public AudioSource audioSource;

    public ControlBotSierra estatico;
    
    void Start()
    {
        
    }

    
    void Update()
    {

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {

            recibirDañoFuego();
        }

       

    }
    public void Estatico1()
        {
            estatico.agent.speed = 0;
        }
    public void recibirDañoElectrico()
    {
        hp = hp - 50;

        if (hp <= 0)
        {

            this.desaparecer();
        }
        GestorDeAudio.instancia.ReproducirSonido("robot daño");

    }

    public void recibirDañoFuego()
    {
        hp = hp - 70;

        if (hp <= 0)
        {

            this.desaparecer();
        }
        GestorDeAudio.instancia.ReproducirSonido("robot daño");

      
    }
    public void recibirDaño()
    {
            hp = hp - 35;

            if (hp <= 0)
            {
                
                this.desaparecer();
            }
        GestorDeAudio.instancia.ReproducirSonido("robot daño");
    }
    private void desaparecer()
    { 
            
            Invoke("MostrarExplosion", 0);
            audioSource.Play();
            Destroy(gameObject);
            Destroy(padre.gameObject);
            

    }

    public void MostrarExplosion()
    {
            GameObject particulas = Instantiate(particulamuerte, transform.position, Quaternion.identity) as GameObject;
            Destroy(particulas, 1.5f);
            
    }
}

