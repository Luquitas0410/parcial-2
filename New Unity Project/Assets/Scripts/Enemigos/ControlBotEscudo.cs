using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControlBotEscudo : MonoBehaviour
{

    public NavMeshAgent agent;

    public Transform player;

    public LayerMask whatIsGround, whatIsPlayer;

    public GameObject pro;
    public Daño quemar;
    public GameObject explosionBolaDeFuego;

    public float sightRange, attackRange;
    public bool playerInSightRange;


    public GameObject particulaexplosion;

    //disparo
    public GameObject BalaInicio;
    public GameObject BalaPrefab;
    public float BalaVelocidad;
    public float Cadencia = 0.3f;
    private float TiempoCadencia = 0f;

    private void Awake()
    {
        player = GameObject.Find("jugador").transform;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    void Start()
    {
        
    }


    void Update()
    {

        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        
        
        if (playerInSightRange) ChasePlayer();

    }
   
    private void ChasePlayer()
    {

        agent.SetDestination(player.position);
        if (Time.time > TiempoCadencia)
        {
            GameObject BalaTemporal = Instantiate(BalaPrefab, BalaInicio.transform.position, BalaInicio.transform.rotation) as GameObject;
            Rigidbody rb = BalaTemporal.GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * BalaVelocidad);
            Destroy(BalaTemporal, 5);
            TiempoCadencia = Time.time + Cadencia;
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            GestorDeAudio.instancia.ReproducirSonido("impacto bola de fuego");
            Instantiate(explosionBolaDeFuego, new Vector3(pro.transform.position.x, pro.transform.position.y, pro.transform.position.z), Quaternion.identity);
            Destroy(pro, 0f);



            quemar.recibirDañoFuego();
        }
    }

}


