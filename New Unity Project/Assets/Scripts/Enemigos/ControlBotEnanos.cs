using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControlBotEnanos : MonoBehaviour
{

    public NavMeshAgent agent;
    public Transform player;
    public LayerMask whatIsGround, whatIsPlayer;

   
    //deteccion de jugador
    public float sightRange, attackRange;
    public bool playerInSightRange;

    //autodestruccion
    public GameObject pj;
    public GameObject explosionAutoDest;
    public GameObject bot;

    public GameObject pro;
    public Daño quemar;
    public GameObject explosionBolaDeFuego;

    private void Awake()
    {
        player = GameObject.Find("jugador").transform;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    void Start()
    {
       
    }


    void Update()
    {

        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
       

        
        if (playerInSightRange) ChasePlayer();


    }
   
   
    private void ChasePlayer()
    {
        agent.SetDestination(player.position);

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject objetoTocado = pj.gameObject;
            ControlJugador scriptObjetoTocado = (ControlJugador)objetoTocado.GetComponent(typeof(ControlJugador));

            if (scriptObjetoTocado != null)
            {
                scriptObjetoTocado.recibirDañoExplosivo();
            }

            Instantiate(explosionAutoDest, new Vector3(bot.transform.position.x, bot.transform.position.y, bot.transform.position.z), Quaternion.identity);
            Destroy(bot, 0);
            GestorDeAudio.instancia.ReproducirSonido("explosion auto destruccion");
        }
        if (collision.gameObject.CompareTag("bala"))
        {
            GestorDeAudio.instancia.ReproducirSonido("impacto bola de fuego");
            Instantiate(explosionBolaDeFuego, new Vector3(pro.transform.position.x, pro.transform.position.y, pro.transform.position.z), Quaternion.identity);
            Destroy(pro, 0f);



            quemar.recibirDañoFuego();
        }
    }
    
   

}

