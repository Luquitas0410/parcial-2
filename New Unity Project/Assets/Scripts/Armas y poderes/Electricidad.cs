using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Electricidad : MonoBehaviour
{
    public Camera camaraPrimeraPersona;
    public float Cadencia = 0.5f;
    private float TiempoCadencia = 0f;
    public GameObject particula, arma;
    LineRenderer laserLineRenderer;
    

    

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        GestorDeAudio.instancia.ReproducirSonido("electricidad");

        laserLineRenderer = gameObject.GetComponent<LineRenderer>();
        laserLineRenderer.startWidth = 0.5f;
        laserLineRenderer.endWidth = 0.01f;
        laserLineRenderer.enabled = false;
    }

   
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            if (Time.time > TiempoCadencia)
            {
                TiempoCadencia = Time.time + Cadencia;

                Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

                RaycastHit hit;



                if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 10000)
                {

                    GestorDeAudio.instancia.ReproducirSonido("rayo electrico");


                    if (hit.collider.name.Substring(0, 3) == "bot")
                    {
                        GameObject objetoTocado = GameObject.Find(hit.transform.name);
                        Da�o scriptObjetoTocado = (Da�o)objetoTocado.GetComponent(typeof(Da�o));

                        if (scriptObjetoTocado != null)
                        {
                            scriptObjetoTocado.recibirDa�oElectrico();

                        }

                        GameObject objetoTocado2 = GameObject.Find(hit.transform.name);
                        Da�o scriptObjetoTocado2 = (Da�o)objetoTocado2.GetComponent(typeof(Da�o));

                        if (scriptObjetoTocado2 != null)
                        {
                            scriptObjetoTocado2.Estatico1();

                        }

                    }

                    

                   

                }
                
                laserLineRenderer.SetPosition(0, arma.transform.position);
                laserLineRenderer.SetPosition(1, hit.point);
                laserLineRenderer.enabled = true;
                StartCoroutine(Retardo());

                if ((Physics.Raycast(ray, out hit) == false))
                {

                    laserLineRenderer.enabled = false;

                    GestorDeAudio.instancia.ReproducirSonido("rayo electrico fallado");
                }

                



            }
            IEnumerator Retardo()
            {
                yield return new WaitForSeconds(0.6f);
                laserLineRenderer.enabled = false;
            }

        }
    }
}
