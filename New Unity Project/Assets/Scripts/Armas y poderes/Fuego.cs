using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fuego : MonoBehaviour
{
    public Camera camaraPrimeraPersona;
    public GameObject proyectil, arma;

    public float Cadencia;
    private float TiempoCadencia = 0f;

    public GameObject Rafaga;

    void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("fuego");
    }

  
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Time.time > TiempoCadencia)
            {
                Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0,0, 0));

                GameObject pro;
                pro = Instantiate(proyectil, arma.transform.position, transform.rotation);

                Rigidbody rb = pro.GetComponent<Rigidbody>();
                rb.AddForce(camaraPrimeraPersona.transform.forward * 30, ForceMode.Impulse);

                Destroy(pro, 20);

                TiempoCadencia = Time.time + Cadencia;

                GestorDeAudio.instancia.ReproducirSonido("bola de fuego");

                GameObject Salida = Instantiate(Rafaga, arma.transform.position, arma.transform.rotation) as GameObject;
                Rigidbody fire = Salida.GetComponent<Rigidbody>();
                 // Destroy (Rafaga, 0.6f);
            }
        }


    }
}
