using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Arma1 : MonoBehaviour
{
    private Camera camaraPrimeraPersona;
    public float Cadencia = 0.5f;
    private float TiempoCadencia = 0f;
    public GameObject particula, arma;
    LineRenderer laserLineRenderer;

    public int balas;
    public int balasTotal;

    public TMPro.TMP_Text balasTXT;
    public TMPro.TMP_Text balasTotalTXT;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        laserLineRenderer = gameObject.GetComponent<LineRenderer>();
        laserLineRenderer.startWidth = 0.1f;
        laserLineRenderer.endWidth = 0.01f;
      
    }


    void Update()
    {
        if (balas <= 0) 
        { 
             if (Input.GetKeyDown(KeyCode.R))
             {
                balas += balasTotal;
                GestorDeAudio.instancia.ReproducirSonido("recarga");
             }
        }
        if (Input.GetMouseButtonDown(1))
        {

            if (Time.time > TiempoCadencia)
            {
                if (balas <= 0)
                {
                    GestorDeAudio.instancia.ReproducirSonido("sinbalas");
                }

                if (balas > 0)
                {
                    
                    balas -= 1;

                    TiempoCadencia = Time.time + Cadencia;

                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                    RaycastHit hit;



                    if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 10000)
                    {

                        GestorDeAudio.instancia.ReproducirSonido("disparo arma 1");
                        Instantiate(particula, hit.transform.position, Quaternion.identity);

                        if (hit.collider.name.Substring(0, 3) == "bot")
                        {
                            GameObject objetoTocado = GameObject.Find(hit.transform.name);
                            Da�o scriptObjetoTocado = (Da�o)objetoTocado.GetComponent(typeof(Da�o));

                            if (scriptObjetoTocado != null)
                            {
                                scriptObjetoTocado.recibirDa�o();
                            }
                        }
                        GameObject newParticula = Instantiate(particula, hit.point, Quaternion.identity);
                        Destroy(newParticula, 4);

                        if (hit.collider.name.Substring(0, 3) == "can")
                        {

                            GameObject objetoTocado = GameObject.Find(hit.transform.name);
                            AbrirPuerta scriptObjetoTocado = (AbrirPuerta)objetoTocado.GetComponent(typeof(AbrirPuerta));

                            if (scriptObjetoTocado != null)
                            {
                                scriptObjetoTocado.AbrirCandado();
                            }

                        }
                    }

                    laserLineRenderer.SetPosition(0, arma.transform.position);
                    laserLineRenderer.SetPosition(1, hit.point);
                    laserLineRenderer.enabled = true;
                    StartCoroutine(Retardo());

                    if ((Physics.Raycast(ray, out hit) == false))
                    {

                        laserLineRenderer.enabled = false;

                        GestorDeAudio.instancia.ReproducirSonido("disparo fallado");
                    }

                }
            }
                IEnumerator Retardo()
                {
                    yield return new WaitForSeconds(0.1f);
                    laserLineRenderer.enabled = false;
                }
          
        }

        balasTXT.text = balas.ToString();
        balasTotalTXT.text = balasTotal.ToString();
    }

}


